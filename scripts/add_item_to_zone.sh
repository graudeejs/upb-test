#!/bin/sh

ENDPOINT='http://localhost:3000/api/zone_items'

if [ $# -ne 2 ]; then
    echo "need ItemID and ZoneID as arguments" > /dev/stderr
    exit 1
fi
curl --data "item_id=$1&zone_id=$2" "$ENDPOINT"
