class CreateWearhouses < ActiveRecord::Migration[5.0]
  def change
    create_table :wearhouses do |t|
      t.string :name

      t.timestamps
    end
  end
end
