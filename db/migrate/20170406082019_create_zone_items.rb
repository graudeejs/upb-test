class CreateZoneItems < ActiveRecord::Migration[5.0]
  def change
    create_table :zone_items do |t|
      t.integer :item_id, index: true
      t.integer :zone_id, index: true
    end

    add_index :zone_items, [:zone_id, :item_id]
  end
end
