# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if AdminUser.count.zero?
  AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
end

%w[
  armatura
  bloks
  logs
  stikls
  panelis
  sija
  durvis
  ramjis
  grida
  jumts
  caurule
  pods
  izlietne
  linolejs
  paklajs
].each do |item_name|
  Item.create!(name: item_name)
end
