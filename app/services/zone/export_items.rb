class Zone::ExportItems
  def initialize(zone)
    @zone = zone
  end

  def call
    workbook.add_worksheet(name: "#{@zone.name} items") do |sheet|
      sheet.add_row ['Item Name', 'Item id']
      @zone.items.find_each do |item|
        sheet.add_row([
          item.name,
          item.id.to_s
        ])
      end
    end

    package
  end

  private

  def package
    @package ||= Axlsx::Package.new
  end

  def workbook
    @workbook ||= package.workbook
  end

end
