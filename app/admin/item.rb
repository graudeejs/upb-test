ActiveAdmin.register Item do
  permit_params :name

  show do
    h3 item.name
    div do
      item.to_qrcode.as_svg.html_safe
    end
  end
end
