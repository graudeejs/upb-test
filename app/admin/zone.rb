ActiveAdmin.register Zone do
  permit_params :name, :wearhouse_id

  show do
    h3 zone.name
    div do
      zone.to_qrcode.as_svg.html_safe
    end

    div do
      link_to('Export items', url_for(action: :export_items, format: :xlsx))
    end
  end

  member_action :export_items, method: :get do
    zone = Zone.find(params[:id])
    respond_to do |format|
      format.xlsx do
        export = Zone::ExportItems.new(zone).call
        send_data export.to_stream.read
      end
    end
  end
end
