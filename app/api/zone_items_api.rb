class ZoneItemsAPI < Grape::API

  resource :zone_items do
    desc 'Add item to zone', http_codes: [
      { code: 200, message: 'success' },
      { code: 400, message: 'TODO' },
      { code: 404, message: 'Resource not found' }
    ]
    params do
      requires :zone_id, type: String, desc: 'Zone ID'
      requires :item_id, type: String, desc: 'Item ID'
    end
    post do
      zone = Zone.find(params[:zone_id])
      item = Item.find(params[:item_id])
      ZoneItem.find_or_create_by(
        zone_id: zone.id,
        item_id: item.id
      ).save!
    end


    desc 'Remove item from zone', http_codes: [
      { code: 200, message: 'success' },
      { code: 400, message: 'TODO' },
      { code: 404, message: 'Resource not found' }
    ]
    params do
      requires :zone_id, type: String, desc: 'Zone ID'
      requires :item_id, type: String, desc: 'Item ID'
    end
    delete do
      ZoneItem.find_by!(
        zone_id: params[:zone_id],
        item_id: params[:item_id]
      ).destroy
    end
  end
end
