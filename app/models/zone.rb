class Zone < ApplicationRecord
  include QrEncodable

  belongs_to :wearhouse
  has_many :zone_items, dependent: :destroy
  has_many :items, through: :zone_items

  validates_presence_of :name, :wearhouse
  validates_uniqueness_of :name
end
