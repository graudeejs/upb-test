class Wearhouse < ApplicationRecord
  has_many :zones, dependent: :destroy

  validates_presence_of :name
  validates_uniqueness_of :name
end
