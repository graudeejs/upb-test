class ZoneItem < ApplicationRecord
  belongs_to :zone
  belongs_to :item

  validates_presence_of :zone, :item
end
