class Item < ApplicationRecord
  include QrEncodable
  has_many :zone_items, dependent: :destroy
  has_many :zones, through: :zone_items

  validates_presence_of :name
  validates_uniqueness_of :name
end
