module QrEncodable
  def to_qrcode
    RQRCode::QRCode.new(string_for_qrcode)
  end

  private

  def string_for_qrcode
    {
      type: self.class.name,
      id: id,
      name: name
    }.to_json
  end
end
